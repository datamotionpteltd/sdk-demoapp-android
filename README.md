# Telematics demo app

This is an app that demostrate using of Telematics SDK. The app will track the person's driving behavior such as speeding, turning, braking and several other things.

# Installation
  - clone this repository to local folder
  - open project with Android Studio
  - Open MainActivity.kt file and setup "YOUR_TOKEN" with your real device token:
    ```
    private const val YOUR_TOKEN = ""  // set your token
    ```   
  - build project and run
  - Click on `START PERMISSIONS WIZARD` or `START PERMISSIONS DIALOG` and grant all required permissions
  - Click on `ENABLE SDK` and you are ready to go!